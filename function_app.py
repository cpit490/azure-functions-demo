import azure.functions as func
import logging
import json
from textblob import TextBlob

app = func.FunctionApp(http_auth_level=func.AuthLevel.FUNCTION)
def analyze(text):
    tb = TextBlob(text)
    d = dict()
    d['sentiment'] = 'positive' if tb.sentiment.polarity > 0 else 'negative'
    d['polarity'] = tb.sentiment.polarity
    d['subjectivity'] = tb.sentiment.subjectivity
    return d

@app.route(route="sentiment_analysis_func")
def sentiment_analysis_func(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    text = req.params.get('text')
    if not text:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            text = req_body.get('text')

    if text:
        resp = json.dumps(analyze(text))
        return func.HttpResponse(resp)
    else:
        return func.HttpResponse(
             "422 - Missing parameter: The required parameter text should be passed in the query parameter or in the request body.",
             status_code=422
        )